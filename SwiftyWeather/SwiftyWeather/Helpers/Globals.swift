//
//  Globals.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import UIKit

enum AppColors {
    
    static var background: UIColor {
        return UIColor(red: 25/255.0, green: 29/255.0, blue: 32/255.0, alpha: 1.0)
    }
    
    static var darkBlueGray : UIColor {
        return UIColor(red: 31/255.0, green: 36/255.0, blue: 39/255.0, alpha: 1.0)
    }
    
    static var orange : UIColor {
        return UIColor(red: 245/255.0, green: 130/255.0, blue: 35/255.0, alpha: 1.0)
    }
}

