//
//  ControllersFactory.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

struct ControllersFactory {
    
    static func makeDetailViewController(forCity city: City?, withSavedCity savedCity: SavedCity? = nil) -> DetailWeatherViewController? {
        
        if let detailVC = DetailWeatherViewController.storyboardInstance() {
            if let city = city {
                detailVC.city = city
                detailVC.savedCity = savedCity
            } else if let savedCity = savedCity {
                detailVC.savedCity = savedCity
                detailVC.city = City(name: savedCity.name ?? "", lat: savedCity.lat, lng: savedCity.lng)
            } else {
                return nil
            }

            detailVC.newtworkingService = NetworkingService(mapper: OpenWeatherMapper(), resourceFactory: OpenWeatherResourceFactory())
            return detailVC
        }
        return nil
    }
    
    static func makeCitiesListViewController(with citiesDataController: ListedCitiessDataController) -> CitiestListViewController? {
        if let citiesListViewController = CitiestListViewController.storyboardInstance() {
            citiesListViewController.citiesDataController = citiesDataController
            return citiesListViewController
        }
        return nil 
    }
}
