//
//  DailyWeather.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/1/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

enum WeatherCondition: String {
    case rain
    case clear
    case clouds
    case cloudy
    case storm
}

struct DailyWeather {
    let cityName: String
    let temperature: Float
    let date: Date
    let weatherCondition: WeatherCondition

    let windSpeed: Float
    let rainChance: Int
    let humidity: Int
}

struct OneDayForecast {
    let temperature: Float
    let date: Date
    let weatherCondition: WeatherCondition
}

struct WeatherForecast {
    let list: [OneDayForecast]
}


