//
//  City.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation


struct City {
    let name: String
    let lat: Float
    let lng: Float
}
