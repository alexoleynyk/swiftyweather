//
//  UIImageExtension.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static func getBigWeatherImage(for condition: WeatherCondition) -> UIImage? {
        switch condition {
        case .clear: return UIImage(named: "Weather3_Big")
        case .rain: return UIImage(named: "Weather5_Big")
        case .clouds: return UIImage(named: "Weather1_Big")
        case .cloudy: return UIImage(named: "Weather4_Big")
        default: return UIImage(named: "Weather2_Big")
        }
    }
    
    static func getWeatherImage(for condition: WeatherCondition) -> UIImage? {
        switch condition {
        case .clear: return UIImage(named: "Weather3")
        case .rain: return UIImage(named: "Weather5")
        case .clouds: return UIImage(named: "Weather1")
        case .cloudy: return UIImage(named: "Weather4")
        default: return UIImage(named: "Weather2")
        }
    }
}
