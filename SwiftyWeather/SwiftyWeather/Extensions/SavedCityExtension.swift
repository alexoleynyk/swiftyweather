//
//  SavedCityExtension.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

extension SavedCity {
    var weatherCondition: WeatherCondition {
        if let conditionRawValue = conditionRawValue {
            return WeatherCondition.init(rawValue: conditionRawValue) ?? .clear
        }
        return .clear
    }
}
