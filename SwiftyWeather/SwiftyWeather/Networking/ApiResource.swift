//
//  ApiResource.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/1/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

protocol ApiResource {
    var baseUrl: String { get }
    var basicParams: [String: String] { get }
    var methodPath: String { get }
    var parameters: [String: String] { get }
}

extension ApiResource {
    var url: URL {
        let url = baseUrl + methodPath
        let basicQueryItems = basicParams.map { URLQueryItem(name: $0.key, value: $0.value) }
        let paramsQueryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        
        var urlComponents = URLComponents(string: url)!
        urlComponents.queryItems = basicQueryItems + paramsQueryItems
        let newUrl = urlComponents.url!
        return newUrl
    }
}
