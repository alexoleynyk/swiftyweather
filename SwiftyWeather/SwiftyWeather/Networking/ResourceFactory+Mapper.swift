//
//  NewFile.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

protocol ResourceFactory {
    func createDailyWeatherResource(lat: Float, lng: Float) -> ApiResource
    func createForecastResource(lat: Float, lng: Float) -> ApiResource
}

protocol Mapper {
    func mapDailyWeather(from data: Data) -> DailyWeather
    func mapForecast(from data: Data) -> WeatherForecast
}



