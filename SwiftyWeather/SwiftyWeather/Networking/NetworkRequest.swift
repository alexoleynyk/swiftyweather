//
//  NetworkRequest.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/1/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

struct ApiRequest {
    private let resource: ApiResource
    
    init(resource: ApiResource) {
        self.resource = resource
    }
    
    func load(withCompletion completion: @escaping (Data?) -> Void) {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: resource.url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let data = data else {
                completion(nil)
                return
            }
            completion(data)
        })
        task.resume()
    }
}
