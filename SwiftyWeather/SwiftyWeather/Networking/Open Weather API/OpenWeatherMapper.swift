//
//  OpenWeather.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import SwiftyJSON

final class OpenWeatherMapper: Mapper {
    func mapForecast(from data: Data) -> WeatherForecast {
        return WeatherForecast(withSwiftyJSONData: data)
    }
    
    func mapDailyWeather(from data: Data) -> DailyWeather {
        return DailyWeather(withSwiftyJSONData: data)
    }
}


// Initialization DailyWeather from JSON with SwiftyJSON
fileprivate extension DailyWeather {
    init(withSwiftyJSONData: Data) {
        do {
            let json = try JSON(data: withSwiftyJSONData)
            cityName = json["name"].stringValue
            temperature = json["main"]["temp"].floatValue
            date = Date(timeIntervalSince1970: TimeInterval(json["dt"].intValue) )
            
            weatherCondition = WeatherCondition(from: json["weather"][0]["main"].stringValue)
            
            windSpeed = json["wind"]["speed"].floatValue
            rainChance = json["rain"]["3h"].intValue
            humidity = json["main"]["humidity"].intValue
        } catch {
            print(error)
            cityName = ""
            temperature = 0
            date = Date()
            weatherCondition = .clear
            windSpeed = 0
            rainChance = 0
            humidity = 0
        }
    }
}

// Initialization OneDayForecast from JSON with SwiftyJSON
fileprivate extension OneDayForecast {
    init(withSwiftyJSONData: Data) {
        do {
            let json = try JSON(data: withSwiftyJSONData)
            temperature = json["temp"]["day"].floatValue
            date = Date(timeIntervalSince1970: TimeInterval(json["dt"].intValue) )
            weatherCondition = WeatherCondition(from: json["weather"][0]["main"].stringValue)
        } catch {
            print(error)
            temperature = 0
            date = Date()
            weatherCondition = .clear
        }
    }
}

// Initialization WeatherForecast from JSON with SwiftyJSON
fileprivate extension WeatherForecast {
    init(withSwiftyJSONData: Data) {
        do {
            let json = try JSON(data: withSwiftyJSONData)
            list = try json["list"].arrayValue.map({ (json) -> OneDayForecast in
                return try OneDayForecast(withSwiftyJSONData: json.rawData())
            })
        } catch {
            print(error)
            list = []
        }
    }
}

fileprivate extension WeatherCondition {
    init(from string: String) {
        switch string {
        case "Clear":
            self = .clear
        case "clouds":
            self = .clouds
        default:
            self = .cloudy
        }
    }
}

