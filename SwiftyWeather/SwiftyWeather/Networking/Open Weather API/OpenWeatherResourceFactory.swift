//
//  Resources.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

final class OpenWeatherResourceFactory: ResourceFactory {
    func createDailyWeatherResource(lat: Float, lng: Float) -> ApiResource {
        return OpenWeatherDailyResource(lat: lat, lng: lng)
    }
    func createForecastResource(lat: Float, lng: Float) -> ApiResource {
        return OpenWeatherForecastResource(lat: lat, lng: lng)
    }
}

protocol OpenWeatherApiResource: ApiResource {}
extension OpenWeatherApiResource {
    
    var basicParams: [String : String] {
        return ["APPID": "1934df326baa735f77cbc93aebf63669", "units": "metric"]
    }
    
    var baseUrl: String {
        return "https://api.openweathermap.org/data/2.5/"
    }
}

struct OpenWeatherDailyResource: OpenWeatherApiResource {
    var methodPath = "weather"
    var parameters: [String : String]
    
    init(lat: Float, lng: Float) {
        parameters = [
            "lat": "\(lat)",
            "lon": "\(lng)"
        ]
    }
}

struct OpenWeatherForecastResource: OpenWeatherApiResource {
    var methodPath = "forecast/daily"
    var parameters: [String : String]
    
    init(lat: Float, lng: Float) {
        parameters = [
            "lat": "\(lat)",
            "lon": "\(lng)",
            "cnt": "16"
        ]
    }
}
