//
//  RequestFactory.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/1/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

final class NetworkingService {
    private let mapper: Mapper
    private let resourceFactory: ResourceFactory
    
    init(mapper: Mapper, resourceFactory: ResourceFactory) {
        self.mapper = mapper
        self.resourceFactory = resourceFactory
    }
    
    func getWeather(lat: Float, lng: Float, completion: @escaping (DailyWeather?) -> ()) {
        let resource = resourceFactory.createDailyWeatherResource(lat: lat, lng: lng)
        let apiRequest = ApiRequest(resource: resource)
        
        apiRequest.load { [weak self] (data) in
            if let data = data {
                completion(self?.mapper.mapDailyWeather(from: data))
            } else {
                completion(nil)
            }
        }
    }
    
    func getForecast(lat: Float, lng: Float, completion: @escaping (WeatherForecast?) -> ()) {
        let resource = resourceFactory.createForecastResource(lat: lat, lng: lng)
        let apiRequest = ApiRequest(resource: resource)
        
        apiRequest.load { [weak self] (data) in
            if let data = data {
                completion(self?.mapper.mapForecast(from: data))
            } else {
                completion(nil)
            }
        }
    }
}
