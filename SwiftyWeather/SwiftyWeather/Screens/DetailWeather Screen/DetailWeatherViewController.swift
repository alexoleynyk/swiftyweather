//
//  DetailWeatherViewController
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit

final class DetailWeatherViewController: UIViewController {
    
    @IBOutlet private weak var mainWeatherInfoView: MainWeatherInfoView!
    @IBOutlet private weak var forecastCollectionView: UICollectionView!
    
    private let cellId = "ForecastID"
    
    var newtworkingService: NetworkingService?
    var savedCitiesDataController: SavedCitiesDataController!
    var forecast: [OneDayForecast] = []
    var currenWeather: DailyWeather?
    
    var city: City!
    var savedCity: SavedCity?
    
    static func storyboardInstance() -> DetailWeatherViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? DetailWeatherViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupForecastCollection()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        newtworkingService?.getWeather(lat: city.lat, lng: city.lng, completion: { (dailyWeather) in
            guard let dailyWeather = dailyWeather else { return }
            self.mainWeatherInfoView.viewModel = MainWeatherInfoView.ViewModel(weather: dailyWeather)
            self.navigationItem.title = dailyWeather.cityName
            self.city = City(name: dailyWeather.cityName, lat: self.city.lat, lng: self.city.lng)
            self.savedCity?.conditionRawValue = dailyWeather.weatherCondition.rawValue
            self.currenWeather = dailyWeather
            self.savedCitiesDataController.save()
            
            UIView.animate(withDuration: 0.3, animations: {
                self.mainWeatherInfoView.layer.opacity = 1
            })
            
            self.newtworkingService?.getForecast(lat: self.city.lat, lng: self.city.lng, completion:  { (forecast) in
                guard let forecast = forecast else { return }

                self.forecast = forecast.list
                self.forecastCollectionView.reloadData()
                UIView.animate(withDuration: 0.3, animations: {
                    self.forecastCollectionView.layer.opacity = 1
                })
            })
        })
    }
    
    private func setupForecastCollection() {
        
        forecastCollectionView.dataSource = self
        forecastCollectionView.register(UINib(nibName: "ForecastCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        
        // setting up layout for collection 
        let cellWidth : CGFloat = ((UIScreen.main.bounds.width ) / 5) - 5
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: cellWidth , height: UIScreen.main.bounds.height * 0.18 - 5)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 2.0
        layout.scrollDirection = .horizontal
        self.forecastCollectionView.setCollectionViewLayout(layout, animated: false)
    }
    
    private func setupUI() {

        self.view.backgroundColor = AppColors.background
        self.mainWeatherInfoView.layer.opacity = 0
        self.forecastCollectionView.layer.opacity = 0
        self.navigationItem.title = city.name
        
        savedCity != nil ? setDeleteButton() : setAddButton()
    }
    
    private func setAddButton() {
        let barItem = UIBarButtonItem(image: UIImage(named: "Add"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(saveAction))
        self.navigationItem.rightBarButtonItems = [barItem]
    }
    
    private func setDeleteButton() {
        let barItem = UIBarButtonItem(image: UIImage(named: "Delete"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(deleteAction))
        self.navigationItem.rightBarButtonItems = [barItem]
    }
    
    @IBAction func saveAction() {
        savedCity = savedCitiesDataController.add(city: city)
        savedCity?.conditionRawValue = currenWeather?.weatherCondition.rawValue
        savedCitiesDataController.save()
        setDeleteButton()
    }
    
    @IBAction func deleteAction() {
        if let savedCity = savedCity {
            savedCitiesDataController.remove(savedCity: savedCity)
            self.savedCity = nil
            setAddButton()
        }
    }

}

extension DetailWeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ForecastCollectionViewCell
        cell.configure(with: forecast[indexPath.row])

        return cell
    }

}
