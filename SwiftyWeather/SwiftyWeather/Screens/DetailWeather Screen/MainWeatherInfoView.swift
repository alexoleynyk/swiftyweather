//
//  MainWeatherInfoView.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import UIKit

final class MainWeatherInfoView: UIView {
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var windSpeedLabel: UILabel!
    @IBOutlet private weak var humidityLabel: UILabel!
    @IBOutlet private weak var rainChanceLabel: UILabel!
    @IBOutlet private weak var conditionWeather: UIImageView!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            temperatureLabel.text = viewModel.temperature
            windSpeedLabel.text = viewModel.windSpeed
            humidityLabel.text = viewModel.humidity
            rainChanceLabel.text = viewModel.rainChance
            conditionWeather.image = viewModel.weatherConditionImage
        }
    }
}

extension MainWeatherInfoView {
    struct ViewModel {
        let temperature: String
        let windSpeed: String
        let humidity: String
        let rainChance: String
        let weatherConditionImage: UIImage?
    }
}

extension MainWeatherInfoView.ViewModel {
    init(weather: DailyWeather) {
        temperature = "\(Int(weather.temperature))°C"
        windSpeed = "\(weather.windSpeed) m/s"
        humidity = "\(weather.humidity)%"
        rainChance = "\(weather.rainChance)%"
        weatherConditionImage = UIImage.getBigWeatherImage(for: weather.weatherCondition)
    }
    
    init() {
        temperature = ""
        windSpeed = ""
        humidity = ""
        rainChance = ""
        weatherConditionImage = nil
    }
}
