//
//  ForecastCollectionViewCell.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = AppColors.darkBlueGray
    }
    
    func configure(with dayForecast: OneDayForecast) {
        temperatureLabel.text = "\(Int(dayForecast.temperature))°"
        imageView.image = UIImage.getBigWeatherImage(for: dayForecast.weatherCondition)
        dayLabel.text = dayForecast.date.dayOfWeek
    }
    
    func getImage() -> UIImage {
        return UIImage(named: "Location")!
    }
}
