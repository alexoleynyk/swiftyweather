//
//  CitiestListViewController.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit

final class CitiestListViewController: UIViewController {
    
    @IBOutlet private weak var citiesTable: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private let cellId = "CityCellID"

    var citiesDataController: ListedCitiessDataController!
    
    static func storyboardInstance() -> CitiestListViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? CitiestListViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        citiesDataController.delegate = self
        searchBar.delegate = self
//        citiesDataController.loadAll()
        setupUI()
        setupCitiesTable()
    }
    
    private func setupCitiesTable() {
        citiesTable.dataSource = self
        citiesTable.delegate = self
        citiesTable.register(UINib(nibName: "CityTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
    }
    
    private func setupUI() {
        view.backgroundColor = AppColors.background
        citiesTable.separatorColor = AppColors.darkBlueGray
        searchBar.barTintColor = AppColors.background
        
        if !citiesDataController.isLoadingDone {
            citiesTable.isHidden = true
        } else {
            activityIndicator.stopAnimating()
        }
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = AppColors.darkBlueGray
    }

}

// Navigation
extension CitiestListViewController {
    
    func openDetailsViewController(forCity city: City) {
        if let detailViewController = ControllersFactory.makeDetailViewController(forCity: city) {
            detailViewController.savedCitiesDataController = SavedCitiesDataController(container: PersistanceService.persistentContainer)
            self.view.endEditing(true)
//            self.citiesDataController.filterCities(with: "")
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
}

extension CitiestListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesDataController.allCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! CityTableViewCell
        cell.cityName.text = citiesDataController.allCities[indexPath.row].name
        return cell
    }
  
}

extension CitiestListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let listCity = citiesDataController.allCities[indexPath.row]
        openDetailsViewController(forCity: City(name: listCity.name ?? "qweqwe", lat: listCity.lat, lng: listCity.lng))
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension CitiestListViewController: ListedCitiessDataControllerDelegate {
    func citiesDataController(_ controller: ListedCitiessDataController, didFinishLoadingWith cities: [ListedCity]) {
        self.citiesTable.reloadData()
        citiesTable.isHidden = false
        self.activityIndicator.stopAnimating()
    }
    
    func citiesDataController(_ controller: ListedCitiessDataController, didFinishFilteringWith cities: [ListedCity]) {
        self.citiesTable.reloadData()
    }
}

extension CitiestListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.citiesDataController.load(withFilter: searchText)
        citiesTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}
