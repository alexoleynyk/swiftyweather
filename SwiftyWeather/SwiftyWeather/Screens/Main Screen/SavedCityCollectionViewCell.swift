//
//  SavedCityCollectionViewCell.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit

class SavedCityCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var cityNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = AppColors.darkBlueGray
    }
    
    func configure(with city: SavedCity) {
        cityNameLabel.text = city.name
        imageView.image = UIImage.getWeatherImage(for: WeatherCondition(rawValue: city.conditionRawValue ?? "clear") ?? .clear)
    }
    
    func getImage() -> UIImage {
        return UIImage(named: "Location")!
    }
}
