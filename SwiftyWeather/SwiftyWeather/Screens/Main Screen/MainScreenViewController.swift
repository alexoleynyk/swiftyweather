//
//  MainScreenViewController.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit
import CoreLocation

final class MainScreenViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    var savedCitiesDataController: SavedCitiesDataController!
    var citiesDataController: ListedCitiessDataController!
    
    private let cellId = "SavedCity"
    private let locationManager = CLLocationManager()
    private var currentCity: City?

    static func storyboardInstance() -> MainScreenViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? MainScreenViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UINib(nibName: "SavedCityCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        setupUI()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        savedCitiesDataController.load()
        collectionView.reloadData()
    }

    private func setupUI() {
        self.collectionView.backgroundColor = AppColors.background
        
        self.navigationItem.rightBarButtonItems = []
        
        // setting up layout for collection
        let cellWidth : CGFloat = ((UIScreen.main.bounds.width ) / 3) - 7
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: cellWidth , height: cellWidth)
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.minimumLineSpacing = 5.0
        layout.minimumInteritemSpacing = 5.0
        self.collectionView.setCollectionViewLayout(layout, animated: false)
        
        let barItem = UIBarButtonItem(image: UIImage(named: "Add"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(openCitySelection))
        self.navigationItem.leftBarButtonItems = [barItem]
    }
    
    @objc func getWeatherForLocationTapped() {
        if let currentCity = currentCity {
            openDetailsViewController(forCity: currentCity)
        }
    }
    
    @objc func openCitySelection() {
        openCitiesListsViewController()
    }
}

// Navigation
extension MainScreenViewController {
    
    func openDetailsViewController(forCity city: City?, withSavedCity savedCity: SavedCity? = nil) {
        if let detailViewController = ControllersFactory.makeDetailViewController(forCity: city, withSavedCity: savedCity) {
            detailViewController.savedCitiesDataController = savedCitiesDataController
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
    
    func openCitiesListsViewController() {
        if let detailViewController = ControllersFactory.makeCitiesListViewController(with: citiesDataController) {
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
}

extension MainScreenViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.delegate = nil
            
            let geolocationBarItem = UIBarButtonItem(image: UIImage(named: "Location"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(getWeatherForLocationTapped))
            self.navigationItem.rightBarButtonItems = [geolocationBarItem]
            
            currentCity = City(name: "CurrentWeatger", lat: Float(location.coordinate.latitude), lng: Float(location.coordinate.longitude))
        }
    }
}

extension MainScreenViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let savedCity = savedCitiesDataController.savedCities[indexPath.row]
        openDetailsViewController(forCity: nil, withSavedCity: savedCity)
    }
}
extension MainScreenViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return savedCitiesDataController.savedCities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SavedCityCollectionViewCell
        cell.configure(with: savedCitiesDataController.savedCities[indexPath.row])
        
        return cell
    }
  
}
