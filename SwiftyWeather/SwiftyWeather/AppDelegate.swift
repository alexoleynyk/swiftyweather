//
//  AppDelegate.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/1/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if !UserDefaults.standard.bool(forKey: "firstLoadDone") {
            preloadSQL()
        }
        
        let savedSitiesDataController = SavedCitiesDataController(container: PersistanceService.persistentContainer)
        
        if !UserDefaults.standard.bool(forKey: "firstLoadDone") {
            savedSitiesDataController.loadDefaults()
            UserDefaults.standard.set(true, forKey: "firstLoadDone")
        }
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.isTranslucent = false
        navigationBarAppearace.barTintColor = AppColors.background
        navigationBarAppearace.tintColor = .white
        navigationBarAppearace.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        
        if let mainVC = MainScreenViewController.storyboardInstance() {
            mainVC.savedCitiesDataController = savedSitiesDataController
            mainVC.citiesDataController = ListedCitiessDataController(container: PersistanceService.persistentContainer)
            
            DispatchQueue.global(qos: .default).async {
                mainVC.citiesDataController.loadAll()
            }
            
            window = UIWindow(frame: UIScreen.main.bounds)
            let nc = UINavigationController(rootViewController: mainVC)
            nc.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            window?.rootViewController = nc
            window?.makeKeyAndVisible()
        }

        return true
    }
    
    func preloadSQL() {
        let sqlitePath = Bundle.main.path(forResource: "SwiftyWeather", ofType: "sqlite")
        
        let sourceURL = URL(fileURLWithPath: sqlitePath!)
        let destinationURL = URL(fileURLWithPath: NSPersistentContainer.defaultDirectoryURL().relativePath + "/SwiftyWeather.sqlite")
        
        do {
            if FileManager.default.fileExists(atPath: destinationURL.absoluteString) {
                try FileManager.default.removeItem(atPath: NSPersistentContainer.defaultDirectoryURL().relativePath + "/SwiftyWeather.sqlite")
            }
            try FileManager.default.copyItem(at: sourceURL, to: destinationURL)
        } catch {
            print(error)
        }
    }
    
    func preloadDataBase() {
        let context = PersistanceService.context
        if let filepath = Bundle.main.path(forResource: "cities", ofType: "json") {
            do {
                let url = URL(fileURLWithPath: filepath)
                let contents = try JSON(data: Data(contentsOf: url))
                for i in 0..<contents.count {
                    let first = contents[i]["name"].stringValue.first!
                    if first >= "A" && first <= "Z" {
                        let city = ListedCity(context: context)
                        city.name = contents[i]["name"].stringValue
                        city.lat = contents[i]["lat"].floatValue
                        city.lng = contents[i]["lng"].floatValue
                    }
                }
                PersistanceService.saveContext()

            } catch {
                print(error)
            }
        } else {
            print("cities.json wasn't found")
        }
        
    }

}

