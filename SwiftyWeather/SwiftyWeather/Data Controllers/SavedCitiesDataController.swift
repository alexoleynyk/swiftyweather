//
//  SavedCitiesDataController.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import CoreData

class SavedCitiesDataController {
    
    private(set) var savedCities: [SavedCity] = []
    private let persistentContainer: NSPersistentContainer!
    private var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    init(container: NSPersistentContainer) {
        self.persistentContainer = container
    }
    
    func createCityInCurrenContext() -> SavedCity {
        let savedCity = SavedCity(context: context)
        return savedCity
    }
    
    func load() {
        let fetchRequest: NSFetchRequest<SavedCity> = SavedCity.fetchRequest()
            var savedCities = [SavedCity]()
            do {
                savedCities = try context.fetch(fetchRequest)
            } catch {}
        self.savedCities = savedCities
    }
    
    func save() {
        do { try context.save() }
        catch { print(error) }
    }
    
    @discardableResult
    func add(city: City) -> SavedCity? {
        let savedCity = SavedCity(context: context)
        savedCity.name = city.name
        savedCity.lat = city.lat
        savedCity.lng = city.lng
        do {
            try context.save()
            return savedCity
        } catch {
            print(error)
            return nil
        }
    }
    
    @discardableResult
    func remove(savedCity: SavedCity) -> SavedCity? {
        context.delete(savedCity)
        do {
            try context.save()
            return savedCity
        } catch {
            print(error)
            return nil
        }
    }
    
    func loadDefaults() {
        self.add(city: City(name: "Odessa", lat: 46.4774, lng: 30.7326))
        self.add(city: City(name: "New York", lat: 40.7142, lng: -74.0059))
    }
}
