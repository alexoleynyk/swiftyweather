//
//  ListedCitiessDataController.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import CoreData

protocol ListedCitiessDataControllerDelegate: class {
    func citiesDataController(_ controller: ListedCitiessDataController, didFinishLoadingWith cities: [ListedCity])
    func citiesDataController(_ controller: ListedCitiessDataController, didFinishFilteringWith cities: [ListedCity])
}

final class ListedCitiessDataController {
    var allCities: [ListedCity] {
        if filteredCities.count > 0 {
            return filteredCities
        } else {
            return cities
        }
    }
    weak var delegate: ListedCitiessDataControllerDelegate?
    private(set) var isLoadingDone = false
    private var cities: [ListedCity] = []
    private var filteredCities: [ListedCity] = []
    
    private let persistentContainer: NSPersistentContainer!
    private var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    init(container: NSPersistentContainer) {
        self.persistentContainer = container
    }
    
    func createCityInCurrenContext() -> ListedCity {
        let savedCity = ListedCity(context: context)
        return savedCity
    }
    
    func loadAll() {
        context.reset()
        let fetchRequest: NSFetchRequest<ListedCity> = ListedCity.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            self.cities = try context.fetch(fetchRequest)
            self.isLoadingDone = true
            DispatchQueue.main.async {
                self.delegate?.citiesDataController(self, didFinishLoadingWith: [])
            }
            
//            return try context.fetch(fetchRequest)
        } catch {
//            return []
        }
    }
    
    func load(withFilter string: String) {
        if string == "" {
            self.filteredCities = []
            return
        }
        let fetchRequest: NSFetchRequest<ListedCity> = ListedCity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name CONTAINS[cd] %@", string)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        do {
            self.filteredCities = try context.fetch(fetchRequest)
        } catch {
            print("Error fetching data from context \(error)")
//            return []
        }
        
    }
}
