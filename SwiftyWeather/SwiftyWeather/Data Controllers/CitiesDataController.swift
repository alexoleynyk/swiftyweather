//
//  CityDataController.swift
//  SwiftyWeather
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

protocol CitiesDataControllerDelegate: class {
    func citiesDataController(_ controller: CitiesDataController, didFinishLoadingWith cities: [City])
    func citiesDataController(_ controller: CitiesDataController, didFinishFilteringWith cities: [City])
}

final class CitiesDataController {
    var allCities: [City] {
        if filteredCities.count > 0 {
            return filteredCities
        } else {
            return cities
        }
    }
    private(set) var isLoadingDone = false
    private var cities: [City] = []
    private var filteredCities: [City] = []
    weak var delegate: CitiesDataControllerDelegate?
    
    func load() {
        if let path = Bundle.main.path(forResource: "cities", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let json = try JSON(data: data)
                
                for json in json.arrayValue {
                    let city = City(withSwiftyJSON: json)
                    cities.append(city)
                }
//                cities = Array(cities.suffix(20)) // remove in production
                cities = cities.sorted(by: { (city1, city2) -> Bool in
                    return city1.name < city2.name
                })
                DispatchQueue.main.async {
                    self.delegate?.citiesDataController(self, didFinishLoadingWith: self.cities)
                }
                isLoadingDone = true
            } catch {
                print(error)
            }
        }
    }
    
    func filterCities(with string: String) {
        if string.count == 0 {
            filteredCities = cities
        } else {
            filteredCities = cities.filter({$0.name.lowercased().prefix(string.count) == string.lowercased()
            })
        }
        
        DispatchQueue.main.async {
            self.delegate?.citiesDataController(self, didFinishFilteringWith: self.filteredCities)
        }
    }
}

fileprivate extension City {
    init(withSwiftyJSON json: JSON) {
        name = json["name"].stringValue
        lat = json["lat"].floatValue
        lng = json["lng"].floatValue
    }
}
