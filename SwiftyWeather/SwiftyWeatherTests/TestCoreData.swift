//
//  TestCoreData.swift
//  SwiftyWeatherTests
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import XCTest
import CoreData
@testable import SwiftyWeather

class TestCoreData: XCTestCase {

    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "SwiftyWeather")
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == NSInMemoryStoreType )
            if let error = error {
                fatalError("Create an in-mem coordinator failed \(error)")
            }
        }
        return container
    }()
    
    func test_SavedCityDataController_createdWithNoData() {
        let sut = SavedCitiesDataController(container: container)
        sut.load()
        
        XCTAssert(sut.savedCities.isEmpty == true)
    }
    
    func test_SavedCityDataController_canAddCity() {
        let sut = SavedCitiesDataController(container: container)
        sut.add(city: City(name: "London", lat: 123, lng: 312))
        sut.load()
        
        XCTAssert(sut.savedCities.isEmpty == false)
        XCTAssertEqual(sut.savedCities.first?.name, "London")
    }
    
    func test_SavedCityDataController_canLoadDefaults() {
        let sut = SavedCitiesDataController(container: container)
        sut.loadDefaults()
        sut.load()
        
        XCTAssert(sut.savedCities.isEmpty == false)
        XCTAssertEqual(sut.savedCities.first?.name, "Odessa")
        XCTAssertEqual(sut.savedCities.last?.name, "New York")
    }
    
    func test_SavedCityDataController_canRemoveCity() {
        let sut = SavedCitiesDataController(container: container)
        let cityForDelete = sut.add(city: City(name: "London", lat: 123, lng: 312))
        sut.add(city: City(name: "Kiev", lat: 123, lng: 312))
        sut.remove(savedCity: cityForDelete!)
        sut.load()
        
        XCTAssertEqual(sut.savedCities.count, 1)
        XCTAssertEqual(sut.savedCities.first?.name, "Kiev")
    }

}
