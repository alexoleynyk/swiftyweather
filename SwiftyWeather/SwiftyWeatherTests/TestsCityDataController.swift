//
//  TestsCityDataController.swift
//  SwiftyWeatherTests
//
//  Created by Alex Oleynyk on 12/2/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import XCTest
@testable import SwiftyWeather

class TestsCityDataController: XCTestCase {

    func testCityDataController_loadData() {
        
        let sut = CitiesDataController()
        sut.load()
        XCTAssert(sut.isLoadingDone == true)
        XCTAssert(sut.allCities.isEmpty == false)
    }
    
    func testCityDataController_loadData_withGCD() {
        let sut = CitiesDataController()
        let loadExpectation = expectation(description: "lodaing data in other thread")
        
        DispatchQueue.global(qos: .default).async {
            sut.load()
            loadExpectation.fulfill()
            XCTAssert(sut.isLoadingDone == true)
            XCTAssert(sut.allCities.isEmpty == false)
        }
        
        XCTAssert(sut.isLoadingDone == false)
        
        waitForExpectations(timeout: 60, handler: nil)
    }

}
