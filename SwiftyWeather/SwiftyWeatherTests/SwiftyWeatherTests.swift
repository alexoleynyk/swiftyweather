//
//  TestModels.swift
//  SwiftyWeatherTests
//
//  Created by Alex Oleynyk on 12/1/18.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import XCTest
@testable import SwiftyWeather

class TestsOpenWeatherApiAndMapper: XCTestCase {
    
    func test_DailyWeatherCreatedFromJSONData_withOpenWeatherMapper() {
        let jsonString = """
        {\"coord\":{\"lon\":139,\"lat\":35},
        \"sys\":{\"country\":\"JP\",\"sunrise\":1369769524,\"sunset\":1369821049},
        \"weather\":[{\"id\":804,\"main\":\"clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],
        \"main\":{\"temp\":289.5,\"humidity\":89,\"pressure\":1013,\"temp_min\":287.04,\"temp_max\":292.04},
        \"wind\":{\"speed\":7.31,\"deg\":187.002},
        \"rain\":{\"3h\":0},
        \"clouds\":{\"all\":92},
        \"dt\":1369824698,
        \"id\":1851632,
        \"name\":\"Shuzenji\",
        \"cod\":200}
        """
        let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false)
        
        let sut = OpenWeatherMapper().mapDailyWeather(from: dataFromString!)
        
        XCTAssertEqual(sut.cityName, "Shuzenji")
        XCTAssertEqual(sut.temperature, 289.5)
        XCTAssertEqual(Int(sut.date.timeIntervalSince1970), 1369824698)
        XCTAssertEqual(sut.weatherCondition, .clouds)
        XCTAssertEqual(sut.windSpeed, 7.31)
        XCTAssertEqual(sut.rainChance, 0)
        XCTAssertEqual(sut.humidity, 89)
    }
    
    func test_WeatherForecastCreatedFromJSONData_withOpenWeatherMapper() {
        let jsonString = """
        {\"cod\":\"200\",\"message\":0,\"city\":{\"geoname_id\":1907296,\"name\":\"Tawarano\",\"lat\":35.0164,\"lon\":139.0077,\"country\":\"JP\",\"iso2\":\"JP\",\"type\":\"\",\"population\":0},\"cnt\":10,\"list\":[
            {\"dt\":1485741600,\"temp\":{\"day\":285.51,\"min\":285.51,\"max\":285.51,\"night\":285.51,\"eve\":285.51,\"morn\":285.51},\"pressure\":1013.75,\"humidity\":100,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"sky is clear\",\"icon\":\"01n\"}],\"speed\":5.52,\"deg\":311,\"clouds\":0},
            {\"dt\":1485828000,\"temp\":{\"day\":282.27,\"min\":282.27,\"max\":284.66,\"night\":284.66,\"eve\":282.78,\"morn\":282.56},\"pressure\":1023.68,\"humidity\":100,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"sky is clear\",\"icon\":\"01d\"}],\"speed\":5.46,\"deg\":66,\"clouds\":0},
            {\"dt\":1485914400,\"temp\":{\"day\":284.83,\"min\":283.21,\"max\":285.7,\"night\":284.16,\"eve\":285.49,\"morn\":283.21},\"pressure\":1017.39,\"humidity\":100,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"sky is clear\",\"icon\":\"02d\"}],\"speed\":13.76,\"deg\":260,\"clouds\":8}]}
        """
        let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false)
        let sut = OpenWeatherMapper().mapForecast(from: dataFromString!)
        
        XCTAssertEqual(sut.list.count, 3)
        XCTAssertEqual(sut.list[0].temperature, 285.51)
        XCTAssertEqual(Int(sut.list[0].date.timeIntervalSince1970), 1485741600)
        XCTAssertEqual(sut.list[0].weatherCondition, .clear)
    }
 
    func test_dailyWeatherRequest_withOpenWeatherResources() {
        let sut = NetworkingService(mapper: OpenWeatherMapper(), resourceFactory: OpenWeatherResourceFactory())
        let weatherExpectation = expectation(description: "Loading weatherResource")
        
        sut.getWeather(lat: 42.556, lng: 1.533) { (dailyWeather) in
            weatherExpectation.fulfill()
            XCTAssertNotNil(dailyWeather)
            XCTAssert(dailyWeather?.cityName != "")
        }
        
        waitForExpectations(timeout: 3) { (error) in
            if let error = error { print(error) }
        }
    }
    
    func test_forecastRequest_withOpenWeatherResources() {
        let sut = NetworkingService(mapper: OpenWeatherMapper(), resourceFactory: OpenWeatherResourceFactory())
        let weatherExpectation = expectation(description: "Loading weatherResource")
        
        sut.getForecast(lat: 42.556, lng: 1.533) { (forecast) in
            weatherExpectation.fulfill()
            XCTAssertNotNil(forecast)
            XCTAssert(forecast?.list.isEmpty == false)
            XCTAssert(forecast?.list[0].temperature != 0)
        }
        
        waitForExpectations(timeout: 3) { (error) in
            if let error = error { print(error) }
        }
    }
    
}
