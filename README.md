# Swifty

### Что сделано

- База со списком городов предзагружена в CoreData
- Есть 2 гоорода по умолчанию
- Возможность добавить любой API для погоды. Можно сделать динамический выбор если немного доработать логику.


### Что можно улучшить

- Ускорить поиск по городам и убрать дубликаты из исходного файла
- Вынести делегаты, датасорсы и менеджер локации в отдельные контроллеры данных 
- Добавить анимаций 
- Можно добавить дргой API. При этом не придется менять половину логики приложения. Нужно только создать фабрику API ресурсов и маппер для их конвертации.
- Актуализировать выбор иконки погодного состояния.



